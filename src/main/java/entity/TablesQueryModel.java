package entity;

import java.util.List;

public class TablesQueryModel {
	
	//from sql
	private String formSql;
	
	//表与表的关系部分
	private List<TableRelationModel> tableRelationModelLists;
	
	//查询的字段
	private List<TableFiledModel> tableFiledModels;
	
	//条件字段
	private List<TableConditionModel> tableConditionModels;
	
	//方法中文名
	private String methodName_cn;
	
	//方法实体名
	private String entityName;
	
	//方法实体中文名
	private String entityName_cn;

	public String getFormSql() {
		return formSql;
	}

	public void setFormSql(String formSql) {
		this.formSql = formSql;
	}

	public List<TableRelationModel> getTableRelationModelLists() {
		return tableRelationModelLists;
	}

	public void setTableRelationModelLists(List<TableRelationModel> tableRelationModelLists) {
		this.tableRelationModelLists = tableRelationModelLists;
	}

	public List<TableFiledModel> getTableFiledModels() {
		return tableFiledModels;
	}

	public void setTableFiledModels(List<TableFiledModel> tableFiledModels) {
		this.tableFiledModels = tableFiledModels;
	}

	public List<TableConditionModel> getTableConditionModels() {
		return tableConditionModels;
	}

	public void setTableConditionModels(List<TableConditionModel> tableConditionModels) {
		this.tableConditionModels = tableConditionModels;
	}

	
	public String getMethodName_cn() {
		return methodName_cn;
	}

	public void setMethodName_cn(String methodName_cn) {
		this.methodName_cn = methodName_cn;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityName_cn() {
		return entityName_cn;
	}

	public void setEntityName_cn(String entityName_cn) {
		this.entityName_cn = entityName_cn;
	}

	@Override
	public String toString() {
		return "TablesQueryModel [formSql=" + formSql + ", tableRelationModelLists=" + tableRelationModelLists
				+ ", tableFiledModels=" + tableFiledModels + ", tableConditionModels=" + tableConditionModels + "]";
	}
	
	

}
