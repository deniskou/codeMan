package entity;

public class Parameters {

    private String projectNameVal;

    private String frameWorkVal;

    private String clientFrameWorkVal;

    private String dataBaseTypeVal;

    private String dataBaseIpVal;

    private String dataBasePortVal;

    private String dataBasePwdVal;

    private String dataBaseNameVal;

    private String dataBaseUserNameVal;

    private String dataBaseUrl;

    private String dataBaseDriverClass;

    private String outPathVal;

    private String makeModelVal;

    private String tableName;

    private String jsFrameWork;

    private String themeVal;

    public Parameters() {
        super();
    }

    public String getProjectNameVal() {
        return projectNameVal;
    }

    public void setProjectNameVal(String projectNameVal) {
        this.projectNameVal = projectNameVal;
    }

    public String getFrameWorkVal() {
        return frameWorkVal;
    }

    public void setFrameWorkVal(String frameWorkVal) {
        this.frameWorkVal = frameWorkVal;
    }

    public String getClientFrameWorkVal() {
        return clientFrameWorkVal;
    }

    public void setClientFrameWorkVal(String clientFrameWorkVal) {
        this.clientFrameWorkVal = clientFrameWorkVal;
    }

    public String getDataBaseTypeVal() {
        return dataBaseTypeVal;
    }

    public void setDataBaseTypeVal(String dataBaseTypeVal) {
        this.dataBaseTypeVal = dataBaseTypeVal;
    }

    public String getDataBaseIpVal() {
        return dataBaseIpVal;
    }

    public void setDataBaseIpVal(String dataBaseIpVal) {
        this.dataBaseIpVal = dataBaseIpVal;
    }

    public String getDataBasePortVal() {
        return dataBasePortVal;
    }

    public void setDataBasePortVal(String dataBasePortVal) {
        this.dataBasePortVal = dataBasePortVal;
    }

    public String getDataBasePwdVal() {
        return dataBasePwdVal;
    }

    public void setDataBasePwdVal(String dataBasePwdVal) {
        this.dataBasePwdVal = dataBasePwdVal;
    }

    public String getDataBaseNameVal() {
        return dataBaseNameVal;
    }

    public void setDataBaseNameVal(String dataBaseNameVal) {
        this.dataBaseNameVal = dataBaseNameVal;
    }

    public String getDataBaseUserNameVal() {
        return dataBaseUserNameVal;
    }

    public void setDataBaseUserNameVal(String dataBaseUserNameVal) {
        this.dataBaseUserNameVal = dataBaseUserNameVal;
    }

    public String getOutPathVal() {
        return outPathVal;
    }

    public void setOutPathVal(String outPathVal) {
        this.outPathVal = outPathVal;
    }

    public String getMakeModelVal() {
        return makeModelVal;
    }

    public void setMakeModelVal(String makeModelVal) {
        this.makeModelVal = makeModelVal;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDataBaseUrl() {
        return dataBaseUrl;
    }

    public void setDataBaseUrl(String dataBaseUrl) {
        this.dataBaseUrl = dataBaseUrl;
    }

    public String getDataBaseDriverClass() {
        return dataBaseDriverClass;
    }

    public void setDataBaseDriverClass(String dataBaseDriverClass) {
        this.dataBaseDriverClass = dataBaseDriverClass;
    }

    public String getJsFrameWork() {
        return jsFrameWork;
    }

    public void setJsFrameWork(String jsFrameWork) {
        this.jsFrameWork = jsFrameWork;
    }

    public String getThemeVal() {
        return themeVal;
    }

    public void setThemeVal(String themeVal) {
        this.themeVal = themeVal;
    }

    @Override
    public String toString() {
        return "Parameters [projectNameVal=" + projectNameVal + ", frameWorkVal=" + frameWorkVal
                + ", clientFrameWorkVal=" + clientFrameWorkVal + ", dataBaseTypeVal=" + dataBaseTypeVal
                + ", dataBaseIpVal=" + dataBaseIpVal + ", dataBasePortVal=" + dataBasePortVal + ", dataBasePwdVal="
                + dataBasePwdVal + ", dataBaseNameVal=" + dataBaseNameVal + ", dataBaseUserNameVal="
                + dataBaseUserNameVal + ", dataBaseUrl=" + dataBaseUrl + ", dataBaseDriverClass=" + dataBaseDriverClass
                + ", outPathVal=" + outPathVal + ", makeModelVal=" + makeModelVal + ", tableName=" + tableName
                + ", jsFrameWork=" + jsFrameWork + ",  themeVal=" + themeVal + "]";
    }

}
