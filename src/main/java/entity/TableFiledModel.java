package entity;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import constant.Filed_cn;
import constant.Filed_eng;
import constant.OrderComboBox;
import constant.ServiceTypeComboBox;

public class TableFiledModel {

	private JComboBox<String> comboBox;

	private Filed_eng filed_eng;

	private Filed_cn filed_cn;

	private ServiceTypeComboBox<String> serviceTypeComboBox;

	private JTextField textFiled;

	private OrderComboBox<String> orderComboBox;

	private String tableName;

	private String anotherTableName;

	private String filedText_eng;

	private String anotherFiledName;

	private String filedText_cn;

	private String filedType;

	private Object filedComment;

	private String filedCommentStr;

	private String canSort;

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}

	public Filed_eng getFiled_eng() {
		return filed_eng;
	}

	public void setFiled_eng(Filed_eng filed_eng) {
		this.filed_eng = filed_eng;
	}

	public String getAnotherFiledName() {
		return anotherFiledName;
	}

	public void setAnotherFiledName(String anotherFiledName) {
		this.anotherFiledName = anotherFiledName;
	}

	public Filed_cn getFiled_cn() {
		return filed_cn;
	}

	public void setFiled_cn(Filed_cn filed_cn) {
		this.filed_cn = filed_cn;
	}

	public ServiceTypeComboBox<String> getServiceTypeComboBox() {
		return serviceTypeComboBox;
	}

	public void setServiceTypeComboBox(ServiceTypeComboBox<String> serviceTypeComboBox) {
		this.serviceTypeComboBox = serviceTypeComboBox;
	}

	public JTextField getTextFiled() {
		return textFiled;
	}

	public void setTextFiled(JTextField textFiled) {
		this.textFiled = textFiled;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getAnotherTableName() {
		return anotherTableName;
	}

	public void setAnotherTableName(String anotherTableName) {
		this.anotherTableName = anotherTableName;
	}

	public String getFiledText_eng() {
		return filedText_eng;
	}

	public void setFiledText_eng(String filedText_eng) {
		this.filedText_eng = filedText_eng;
	}

	public String getFiledText_cn() {
		return filedText_cn;
	}

	public void setFiledText_cn(String filedText_cn) {
		this.filedText_cn = filedText_cn;
	}

	public String getFiledType() {
		return filedType;
	}

	public void setFiledType(String filedType) {
		this.filedType = filedType;
	}

	public Object getFiledComment() {
		return filedComment;
	}

	public void setFiledComment(Object filedComment) {
		this.filedComment = filedComment;
	}

	public String getFiledCommentStr() {
		return filedCommentStr;
	}

	public void setFiledCommentStr(String filedCommentStr) {
		this.filedCommentStr = filedCommentStr;
	}

	public OrderComboBox<String> getOrderComboBox() {
		return orderComboBox;
	}

	public void setOrderComboBox(OrderComboBox<String> orderComboBox) {
		this.orderComboBox = orderComboBox;
	}

	public String getCanSort() {
		return canSort;
	}

	public void setCanSort(String canSort) {
		this.canSort = canSort;
	}

	@Override
	public String toString() {
		return "TableFiledModel [tableName=" + tableName + ", anotherTableName=" + anotherTableName + ", filedText_eng="
				+ filedText_eng + ", filedText_cn=" + filedText_cn + ", filedType=" + filedType + ", filedComment="
				+ filedComment + "]";
	}

}
