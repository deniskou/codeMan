package entity;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import constant.CompareComboBox;
import constant.QueryCheckBox;
import constant.ServiceJTextField;
import constant.ServiceTypeComboBox;
import constant.ShowNumJSpinner;
import constant.UpdateCheckBox;

public class DatabaseModel {

	private JCheckBox checkBox;

	private JTextField textField;

	private JSpinner jSpinner;
	
	private ShowNumJSpinner showNumJSpinner;

	private JComboBox<String> comboBox;

	private ServiceTypeComboBox<String> serviceTypeComboBox;

	private ServiceJTextField serviceJTextField;

	private CompareComboBox<String> compareComboBox;

	private UpdateCheckBox updateCheckBox;

	private QueryCheckBox queryCheckBox;

	private String columnsEng;

	private String columnsCn;

	private Integer whereNo;
	
	private Integer showNo;

	private String canSort;

	private String serviceType;

	private Object serviceText;

	private String serviceTextStr;

	private String compareValue;

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public String getColumnsEng() {
		return columnsEng;
	}

	public void setColumnsEng(String columnsEng) {
		this.columnsEng = columnsEng;
	}

	public String getColumnsCn() {
		return columnsCn;
	}

	public void setColumnsCn(String columnsCn) {
		this.columnsCn = columnsCn;
	}

	public JSpinner getjSpinner() {
		return jSpinner;
	}

	public void setjSpinner(JSpinner jSpinner) {
		this.jSpinner = jSpinner;
	}

	public Integer getWhereNo() {
		return whereNo;
	}

	public void setWhereNo(Integer whereNo) {
		this.whereNo = whereNo;
	}

	public ShowNumJSpinner getShowNumJSpinner() {
		return showNumJSpinner;
	}

	public void setShowNumJSpinner(ShowNumJSpinner showNumJSpinner) {
		this.showNumJSpinner = showNumJSpinner;
	}

	public Integer getShowNo() {
		return showNo;
	}

	public void setShowNo(Integer showNo) {
		this.showNo = showNo;
	}

	public String getCanSort() {
		return canSort;
	}

	public void setCanSort(String canSort) {
		this.canSort = canSort;
	}

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}

	public ServiceTypeComboBox<String> getServiceTypeComboBox() {
		return serviceTypeComboBox;
	}

	public void setServiceTypeComboBox(ServiceTypeComboBox<String> serviceTypeComboBox) {
		this.serviceTypeComboBox = serviceTypeComboBox;
	}

	public ServiceJTextField getServiceJTextField() {
		return serviceJTextField;
	}

	public void setServiceJTextField(ServiceJTextField serviceJTextField) {
		this.serviceJTextField = serviceJTextField;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Object getServiceText() {
		return serviceText;
	}

	public void setServiceText(Object serviceText) {
		this.serviceText = serviceText;
	}

	public String getServiceTextStr() {
		return serviceTextStr;
	}

	public void setServiceTextStr(String serviceTextStr) {
		this.serviceTextStr = serviceTextStr;
	}

	public CompareComboBox<String> getCompareComboBox() {
		return compareComboBox;
	}

	public void setCompareComboBox(CompareComboBox<String> compareComboBox) {
		this.compareComboBox = compareComboBox;
	}

	public String getCompareValue() {
		return compareValue;
	}

	public void setCompareValue(String compareValue) {
		this.compareValue = compareValue;
	}

	public UpdateCheckBox getUpdateCheckBox() {
		return updateCheckBox;
	}

	public void setUpdateCheckBox(UpdateCheckBox updateCheckBox) {
		this.updateCheckBox = updateCheckBox;
	}

	public QueryCheckBox getQueryCheckBox() {
		return queryCheckBox;
	}

	public void setQueryCheckBox(QueryCheckBox queryCheckBox) {
		this.queryCheckBox = queryCheckBox;
	}

	@Override
	public boolean equals(Object obj) {
		DatabaseModel model = (DatabaseModel) obj;
		if (this.getColumnsEng().equals(model.getColumnsEng())) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getColumnsEng().length();
	}

}
