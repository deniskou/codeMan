package constant;

public class CodeFtlConstant {

    public static final String dao_ftl = "dao.ftl";

    public static final String Iservice_ftl = "Iservice.ftl";

    public static final String service_ftl = "service.ftl";

    public static final String controller_ftl = "controller.ftl";

    public static final String applicationYml_ftl = "applicationYml.ftl";

    public static final String sqlMapper_ftl = "sqlMapper.ftl";

    public static final String html_ftl = "html.ftl";

    public static final String pom_ftl = "pom.ftl";

    public static final String classpath_ftl = "classpath.ftl";

    public static final String project_ftl = "project.ftl";

    public static final String mainApplication_ftl = "mainApplication.ftl";

    public static final String mainApplicationTest_ftl = "mainApplicationTest.ftl";

    public static final String castutil_ftl = "castutil.ftl";

    public static final String welcome_ftl = "welcome.ftl";

    public static final String home_ftl = "home.ftl";

    public static final String mvcConfig_ftl = "mvcConfig.ftl";

    public static final String baseController_ftl = "baseController.ftl";

    public static final String IBaseService_ftl = "IBaseService.ftl";

    public static final String baseService_ftl = "baseService.ftl";

    public static final String IBaseDao_ftl = "IBaseDao.ftl";

    public static final String excelUtil_ftl = "excelUtil.ftl";

    public static final String transactionAdviceConfig_ftl = "transactionAdviceConfig.ftl";

    public static final String logback_ftl = "logback.ftl";

    public static final String mvcInterceptor_ftl = "mvcInterceptor.ftl";

    public static final String mvcJdbcProperties_ftl = "mvc-jdbcProperties.ftl";

    public static final String mvcSpringMvcXml_ftl = "mvc-spring-mvc.ftl";

    public static final String mvcSpringMybatis_ftl = "mvc-spring-mybatis.ftl";

    public static final String webXml_ftl = "webXml.ftl";

    public static final String orgEclipseComponent_ftl = "orgEclipseComponent.ftl";

    public static final String manifestmf_ftl = "manifestmf.ftl";

    public static final String MutiTableSqlMapper_ftl = "MutiTableSqlMapper.ftl";

    public static final String IMutiTableDao_ftl = "IMutiTableDao.ftl";

    public static final String IMutiTableService_ftl = "IMutiTableService.ftl";

    public static final String MutiTableService_ftl = "MutiTableService.ftl";

    public static final String MutiTableController_ftl = "MutiTableController.ftl";

    public static final String mutiTableHtml_ftl = "mutiTableHtml.ftl";

    public static final String mybatisConfig_ftl = "mybatis-config.ftl";

    public static final String userController_ftl = "userController.ftl";

    public static final String loginHtml_ftl = "loginHtml.ftl";

    public static final String user_ftl = "user.ftl";

    public static final String userMapper_ftl = "userMapper.ftl";

    public static final String IUserService_ftl = "IUserService.ftl";

    public static final String userService_ftl = "userService.ftl";

    public static final String IUserDao_ftl = "IUserDao.ftl";

    public static final String allExceptionHandler_ftl = "allExceptionHandler.ftl";

    public static final String logAopAspect_ftl = "logAopAspect.ftl";

    public static final String loggerUtil_ftl = "loggerUtil.ftl";

    public static final String entity_ftl = "entity.ftl";

    public static final String commonEntity_ftl = "commonEntity.ftl";

    public static final String pageData_ftl = "pageData.ftl";

    public static final String commonResult_ftl = "commonResult.ftl";

    public static final String resultConstant_ftl = "resultConstant.ftl";

    public static final String configJs_ftl = "configJs.ftl";

    public static final String pageUtil_ftl = "pageUtil.ftl";

    public static final String swaggerConfig_ftl = "swaggerConfig.ftl";

	public static final String mutiTableEntity_ftl = "mutiTableEntity.ftl";
}
