package constant;

public class CodeConstant {

    public static String NEW_LINE = System.getProperty("line.separator");

    public static String yesChoose = " √";

    public static final String capturePackageName = "capturePackageName";

    public static final String captureProjectName = "captureProjectName";

    public static final String packageName = "packageName";

    public static final String projectName = "projectName";

    public static final String IDaoName = "IDaoName";

    public static final String IServiceName = "IServiceName";

    public static final String serviceName = "serviceName";

    public static final String controllerName = "controllerName";

    public static final String controllerPrefix = "controllerPrefix";

    public static final String dataBaseUserName = "dataBaseUserName";

    public static final String dataBasePwd = "dataBasePwd";

    public static final String dataBaseUrl = "dataBaseUrl";

    public static final String dataBaseType = "dataBaseType";

    public static final String dataBaseDriverClass = "dataBaseDriverClass";

    public static final String tableName = "tableName";

    public static final String realTableName = "realTableName";

    public static final String columnData = "columnData";

    public static final String columnList = "columnList";

    public static final String selectColumnList = "selectColumnList";

    public static final String updateColumnList = "updateColumnList";

    public static final String selectColumnListMapper = "selectColumnListMapper";

    public static final String primaryKeyList = "primaryKeyList";

    public static final String tableNameList = "tableNameList";

    public static final String queryColumnList = "queryColumnList";

    public static final String currentTableCnName = "currentTableCnName";

    public static final String frameWorkVal = "frameWorkVal";

    public static final String tablesQueryMap = "tablesQueryMap";

    public static final String tablesQueryEndAndCnMap = "tablesQueryEndAndCnMap";

    public static final String currentMutiEng = "currentMutiEng";

    public static final String currentMutiCn = "currentMutiCn";

    public static final String currentMutiMethodEng = "currentMutiMethodEng";

    public static final String currentMutiMethodCn = "currentMutiMethodCn";

    public static final String tableFiledModels = "tableFiledModels";

    public static final String tableConditionModels = "tableConditionModels";

    public static final String jsFrameWork = "jsFrameWork";

    public static final String theme = "theme";

    public static final String clientStyleVal = "clientStyleVal";

    public static final String userCondition = "userCondition";

    public static final String userTable = "userTable";

    public static final String userNameFiled = "userNameFiled";

    public static final String userPwdFiled = "userPwdFiled";

    public static final String loginModel = "loginModel";

    public static final String entityName = "entityName";

    public static final String entityNameAndTypes = "entityNameAndTypes";

    public static final String paramConfigKey = "paramConfigKey";

    public static final String oldTheme = "经典后台Thymleaf版";

    public static final String hAdminTheme = "前后端分离响应式";

    public static final String databasePool = "databasePool";

    public static final String ifUseSwagger = "ifUseSwagger";

    public static final String currentMethodMap = "currentMethodMap";

    public static final String capCurrentMutiEng = "capCurrentMutiEng";

    public static final String currentMutiEntityName = "currentMutiEntityName";

    public static final String currentMutiEntityNameCn = "currentMutiEntityNameCn";

    public static final String entityFiledModels = "entityFiledModels";
}
