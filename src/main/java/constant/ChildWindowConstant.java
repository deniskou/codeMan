package constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeMaker.CallGood;
import codeMaker.CommonParamsConfig;
import codeMaker.DataBaseConfig;
import codeMaker.LoginConfig;
import codeMaker.MakeEntity;
import codeMaker.TablesQuery;
import codeMaker.VxGzhCode;
import entity.CommonParametersModel;
import entity.DatabaseModel;
import entity.MakeEntityModel;
import entity.TableNameAndType;
import entity.TablesQueryModel;

public class ChildWindowConstant {

	public static DataBaseConfig dataBaseConfig;

	public static TablesQuery tablesQuery;

	public static MakeEntity makeEntity;

	public static CallGood callGood;

	public static LoginConfig loginConfig;

	public static VxGzhCode vxGzhCode;

	public static CommonParamsConfig commonParamsConfig;

	// 最终设置的表的主键
	public static Map<String, List<String>> primaryKeyListMap = new HashMap<>();

	// 最终设置的展示的字段的相关信息
	public static Map<String, List<DatabaseModel>> columnMsgMap = new HashMap<>();
	// 最终设置的更新的字段的相关信息
	public static Map<String, List<DatabaseModel>> updateColumnMsgMap = new HashMap<>();
	// 最终设置的查询的字段的相关信息
	public static Map<String, List<DatabaseModel>> queryColumnMsgMap = new HashMap<>();

	// 最终设置的所有字段的相关信息
	public static Map<String, List<DatabaseModel>> allColumnMsgMap = new HashMap<>();

	// 最终设置的表的中文名称
	public static Map<String, String> currentTableCnNameMap = new HashMap<>();

	// 最终设置的表的参数类型（传参是JavaBean还是Map）
	public static Map<String, String> tableParamConfig = new HashMap<>();

	// 最终设置的表的字段类项和名称
	public static Map<String, List<TableNameAndType>> currentTableNameAndTypes = new HashMap<>();

	// public static Map<String, TablesQueryModel> tablesQueryMap = new HashMap<>();

	// 多表查询的配置信息 key:模块名 value: key：方法名 value：对应的sql
	public static Map<String, Map<String, TablesQueryModel>> tablesQueryMap = new HashMap<>();

	// 模块名和模块中文名
	public static Map<String, String> tablesQueryEndAndCnMap = new HashMap<>();

	// 自定义实体
	public static Map<String, List<MakeEntityModel>> makeEntityModelMap = new HashMap<>();
	
	public static Map<String, String> makeEntityEngAndCn = new HashMap<>();
	

	// 静态用户的map
	public static List<String> user1 = new ArrayList<>();
	public static List<String> user2 = new ArrayList<>();
	public static List<String> user3 = new ArrayList<>();

	// 全局常用参数配置
	public static CommonParametersModel commonParametersModel = new CommonParametersModel();

	// 动态用户的数组
	public static List<String> dynamicUserList = new ArrayList<>();

}
