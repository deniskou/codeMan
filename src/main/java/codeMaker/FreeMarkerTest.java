package codeMaker;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import constant.Constant;
import constant.FreeMarkerConfig;
import freemarker.template.Template;
import util.FreeOutUtil;
import util.ZipUtils;

public class FreeMarkerTest {

	public static void main(String[] args) throws Exception {

		ZipUtils.unZip(new File(Constant.modelFilesZip), Constant.modelFiles, Constant.zipPwd);

		System.out.println("解压成功！");

		Template template = FreeMarkerConfig.configuration.getTemplate("/dao.ftl");
		Map<String, String> root = new HashMap<String, String>();
		root.put("packageName", "test");
		root.put("IDaoName", "TestDao");

		String dir = FreeOutUtil.setProjectName(Constant.freeOutPath_controller, "test");

		System.out.println(dir);

		Writer out = new FileWriter(FreeOutUtil.checkAndMakeDir(dir) + "demo.java");
		template.process(root, out);
		System.out.println("转换成功");
		out.flush();
		out.close();

		ZipUtils.deleteFiles(Constant.modelFiles);

	}

}
