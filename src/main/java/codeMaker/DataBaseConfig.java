package codeMaker;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.CompareComboBox;
import constant.QueryCheckBox;
import constant.ServiceJTextField;
import constant.ServiceTypeComboBox;
import constant.ShowNumJSpinner;
import constant.UpdateCheckBox;
import entity.DatabaseModel;
import entity.Parameters;
import entity.TableNameAndType;
import util.DBUtils;

public class DataBaseConfig extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static String currentDataBaseName;
    private static String currentDataBaseUrl;
    private JTextField primaryKeyListText;
    private JTextField currentTableCn;

    /**
     * Launch the application.
     *
     * @param parameters
     */
    public static void main(final Parameters parameters) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    if (currentDataBaseName == null || currentDataBaseUrl == null) {
                        // 记录当前数据库的名称和url
                        currentDataBaseName = parameters.getDataBaseNameVal();
                        currentDataBaseUrl = parameters.getDataBaseUrl();
                    } else {
                        // 如果数据库不一致，清空map
                        if (!currentDataBaseName.equals(parameters.getDataBaseNameVal())
                                || !currentDataBaseUrl.equals(parameters.getDataBaseUrl())) {
                            ChildWindowConstant.columnMsgMap.clear();
                            ChildWindowConstant.queryColumnMsgMap.clear();
                            ChildWindowConstant.updateColumnMsgMap.clear();
                            ChildWindowConstant.allColumnMsgMap.clear();
                            ChildWindowConstant.currentTableCnNameMap.clear();
                            ChildWindowConstant.primaryKeyListMap.clear();
                            // 记录当前数据库的名称和url
                            currentDataBaseName = parameters.getDataBaseNameVal();
                            currentDataBaseUrl = parameters.getDataBaseUrl();
                        }

                    }

                    DataBaseConfig dataBaseConfig = new DataBaseConfig(parameters);
                    ChildWindowConstant.dataBaseConfig = dataBaseConfig;
                    dataBaseConfig.setVisible(true);

                    JOptionPane.showMessageDialog(dataBaseConfig,
                            "查询次序：请按照字段被查询所能筛选掉数据的个数进行设置，把能筛选掉大量数据的字段的按升序设置，将根据设置自动对sql的查询顺序进行优化！"
                                    + CodeConstant.NEW_LINE + "业务类型：请根据字段的具体含义选择业务类型" + CodeConstant.NEW_LINE + "类型描述："
                                    + CodeConstant.NEW_LINE + "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值"
                                    + CodeConstant.NEW_LINE + "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）"
                                    + CodeConstant.NEW_LINE + "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE
                                    + "更新展示：生成的页面点击更新按钮的时候需要展示的字段" + CodeConstant.NEW_LINE + "条件展示：生成的页面上的查询条件需要展示的字段",
                            "提示", JOptionPane.INFORMATION_MESSAGE);

                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * Create the frame.
     *
     * @param
     */
    public DataBaseConfig(Parameters parameters) {

        setResizable(true);
        setTitle("数据库配置");

        setIconImage(Toolkit.getDefaultToolkit().getImage(MakeCode.class.getResource(
                "/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 1297, 730);

        // this.setLocationRelativeTo(null);

        final JButton confirmButton = new JButton("确定");

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportBorder(new CompoundBorder());

        JComboBox<String> tableNameList = new JComboBox<String>();

        JLabel label = new JLabel("选择表");

        JLabel label_1 = new JLabel("当前表主键");

        JLabel label_2 = new JLabel("(复合主键");

        JLabel label_3 = new JLabel("使用#隔开)");

        primaryKeyListText = new JTextField();
        primaryKeyListText.setColumns(10);

        JLabel label_4 = new JLabel("配置完每张表");

        JLabel lblNewLabel = new JLabel("请点击");

        JLabel label_5 = new JLabel("确定按钮！");

        JButton btnNewButton = new JButton("配置说明");
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(btnNewButton.getParent(),
                        "查询次序：请按照字段被查询所能筛选掉数据的个数进行设置，把能筛选掉大量数据的字段的按升序设置，将根据设置自动对sql的查询顺序进行优化！" + CodeConstant.NEW_LINE
                                + "业务类型：请根据字段的具体含义选择业务类型" + CodeConstant.NEW_LINE + "类型描述：" + CodeConstant.NEW_LINE
                                + "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值" + CodeConstant.NEW_LINE
                                + "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）" + CodeConstant.NEW_LINE
                                + "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE + "更新展示：生成的页面点击更新按钮的时候需要展示的字段"
                                + CodeConstant.NEW_LINE + "条件展示：生成的页面上的查询条件需要展示的字段",
                        "提示", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        JLabel label_6 = new JLabel("当前表中文名");

        currentTableCn = new JTextField();
        currentTableCn.setColumns(10);

        JLabel label_7 = new JLabel("参数设置");

        JComboBox<String> paramConfig = new JComboBox<>();

        paramConfig.setModel(new DefaultComboBoxModel<String>(new String[]{"JavaBean", "Map"}));

        GroupLayout groupLayout = new GroupLayout(getContentPane());
        groupLayout.setHorizontalGroup(
        	groupLayout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addGap(20)
        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
        				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
        					.addComponent(label)
        					.addComponent(tableNameList, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
        					.addComponent(label_1)
        					.addComponent(label_2)
        					.addComponent(label_3)
        					.addComponent(primaryKeyListText, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
        					.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
        					.addComponent(lblNewLabel)
        					.addComponent(label_5)
        					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
        					.addComponent(label_6)
        					.addComponent(currentTableCn, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
        					.addComponent(label_7))
        				.addComponent(paramConfig, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addGap(26)
        			.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 1095, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(28, Short.MAX_VALUE))
        		.addGroup(groupLayout.createSequentialGroup()
        			.addContainerGap(673, Short.MAX_VALUE)
        			.addComponent(confirmButton)
        			.addGap(551))
        );
        groupLayout.setVerticalGroup(
        	groupLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(groupLayout.createSequentialGroup()
        					.addGap(50)
        					.addComponent(label)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(tableNameList, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(26)
        					.addComponent(label_1)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(label_2)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(label_3)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(primaryKeyListText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(18)
        					.addComponent(label_4)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(lblNewLabel)
        					.addGap(2)
        					.addComponent(label_5)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(btnNewButton)
        					.addGap(18)
        					.addComponent(label_6)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(currentTableCn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(18)
        					.addComponent(label_7)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(paramConfig, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        				.addGroup(groupLayout.createSequentialGroup()
        					.addGap(30)
        					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 546, GroupLayout.PREFERRED_SIZE)))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(confirmButton)
        			.addContainerGap(87, Short.MAX_VALUE))
        );

        String[] headNameArr = new String[]{"需展示的列", "中文名称", "查询次序", "显示次序", "是否可排序", "业务类型", "类型描述", "比较关系", "更新展示",
                "条件展示"};

        // 动态设置表格选择下拉框
        String tableName = "-请选择-" + "#" + parameters.getTableName();
        String[] tableNameArr = tableName.split("#");
        tableNameList.setModel(new DefaultComboBoxModel<String>(tableNameArr));

        final JPanel columnNamePanel = new JPanel();
        scrollPane.setViewportView(columnNamePanel);

        // 当下拉框发生变化的时候
        tableNameList.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {

                if (e.getStateChange() != 1) {
                    return;
                }
                String currentTableName = tableNameList.getSelectedItem().toString();

                // 获取当前表主键
                List<String> primaryList = ChildWindowConstant.primaryKeyListMap.get(currentTableName);

                // 获取当前表的中文名
                String currentTableCnName = ChildWindowConstant.currentTableCnNameMap.get(currentTableName);

                // 获取表的参数类型
                String paramFiled = ChildWindowConstant.tableParamConfig.get(CodeConstant.paramConfigKey);
                String selectParam = paramFiled == null ? "JavaBean" : paramFiled;

                String primaryKeyStr = "";
                if (primaryList != null) {
                    for (int i = 0; i < primaryList.size(); i++) {
                        if (i == primaryList.size() - 1) {
                            primaryKeyStr += primaryList.get(i);
                            break;
                        }
                        primaryKeyStr += primaryList.get(i) + "#";
                    }
                }

                primaryKeyListText.setText(primaryKeyStr);

                currentTableCn.setText(currentTableCnName == null ? "" : currentTableCnName);

                paramConfig.setSelectedItem(selectParam);

                // 移除所有元素
                columnNamePanel.removeAll();

                // 如果选择了这一个，不做任何操作
                if ("-请选择-".equals(currentTableName)) {
                    ChildWindowConstant.dataBaseConfig.validate();
                    ChildWindowConstant.dataBaseConfig.repaint();
                    return;
                }

                List<DatabaseModel> databaseModelList = ChildWindowConstant.allColumnMsgMap.get(currentTableName);

                // 如果不为null，主键必然不为null 直接遍历全部的列添加即可
                if (databaseModelList != null) {

                    columnNamePanel.setLayout(new GridLayout(0, 10, 10, 10));

                    for (String headName : headNameArr) {
                        JLabel jLabel = new JLabel(headName);
                        columnNamePanel.add(jLabel);
                    }

                    for (int i = 0; i < databaseModelList.size(); i++) {

                        DatabaseModel databaseModel = databaseModelList.get(i);
                        JCheckBox checkBox = databaseModel.getCheckBox();
                        JTextField textField = databaseModel.getTextField();
                        JSpinner jSpinner = databaseModel.getjSpinner();
                        ShowNumJSpinner showNumJSpinner = databaseModel.getShowNumJSpinner();
                        JComboBox<String> comboBox = databaseModel.getComboBox();
                        ServiceTypeComboBox<String> serviceTypeComboBox = databaseModel.getServiceTypeComboBox();
                        ServiceJTextField serviceJTextField = databaseModel.getServiceJTextField();
                        CompareComboBox<String> compareComboBox = databaseModel.getCompareComboBox();
                        UpdateCheckBox updateCheckBox = databaseModel.getUpdateCheckBox();
                        QueryCheckBox queryCheckBox = databaseModel.getQueryCheckBox();

                        columnNamePanel.add(checkBox);
                        columnNamePanel.add(textField);
                        columnNamePanel.add(jSpinner);
                        columnNamePanel.add(showNumJSpinner);
                        columnNamePanel.add(comboBox);
                        columnNamePanel.add(serviceTypeComboBox);
                        columnNamePanel.add(serviceJTextField);
                        columnNamePanel.add(compareComboBox);
                        columnNamePanel.add(updateCheckBox);
                        columnNamePanel.add(queryCheckBox);
                    }

                    ChildWindowConstant.dataBaseConfig.validate();
                    ChildWindowConstant.dataBaseConfig.repaint();
                    return;

                }

                // 如果需要查询数据库，则证明是首次设置，直接按最初的默认值添加即可
                List<String> columnNameList = DBUtils.getColumnNameList(parameters.getDataBaseTypeVal(),
                        currentTableName, DBUtils.getConnection(parameters));
                columnNamePanel.setLayout(new GridLayout(0, 10, 10, 10));

                for (String headName : headNameArr) {
                    JLabel jLabel = new JLabel(headName);
                    columnNamePanel.add(jLabel);
                }

                // 动态展示列名
                for (int i = 0; i < columnNameList.size(); i++) {

                    String columnName = columnNameList.get(i);

                    JCheckBox columnCheckBox = new JCheckBox(columnName);

                    JTextField column_cn = new JTextField();

                    JComboBox<String> comboBox = new JComboBox<>();
                    comboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"否", "是"}));

                    // 字段优先级
                    JSpinner spinner = initJspinner(new JSpinner());

                    JSpinner showNumJSpinner = initJspinner(new ShowNumJSpinner());

                    ServiceTypeComboBox<String> serviceTypeComboBox = new ServiceTypeComboBox<>();
                    serviceTypeComboBox.setModel(
                            new DefaultComboBoxModel<String>(new String[]{"字符串", "数字", "布尔", "状态码", "日期", "文字域"}));
                    ServiceJTextField serviceJTextField = new ServiceJTextField();
                    CompareComboBox<String> compareComboBox = new CompareComboBox<>();
                    compareComboBox.setModel(new DefaultComboBoxModel<String>(
                            new String[]{"=", "like", ">=", "<=", "!=", ">= && <="}));
                    UpdateCheckBox updateCheckBox = new UpdateCheckBox(columnName);
                    QueryCheckBox queryCheckBox = new QueryCheckBox(columnName);

                    columnNamePanel.add(columnCheckBox);
                    columnNamePanel.add(column_cn);
                    columnNamePanel.add(spinner);
                    columnNamePanel.add(showNumJSpinner);
                    columnNamePanel.add(comboBox);
                    columnNamePanel.add(serviceTypeComboBox);
                    columnNamePanel.add(serviceJTextField);
                    columnNamePanel.add(compareComboBox);
                    columnNamePanel.add(updateCheckBox);
                    columnNamePanel.add(queryCheckBox);
                }

                ChildWindowConstant.dataBaseConfig.validate();
                ChildWindowConstant.dataBaseConfig.repaint();
            }

            private JSpinner initJspinner(JSpinner spinner) {
                spinner.setModel(new SpinnerNumberModel(1, 1, 30, 1));
                JSpinner.NumberEditor editor = new JSpinner.NumberEditor(spinner, "0");
                spinner.setEditor(editor);
                JFormattedTextField formattedTextField = ((JSpinner.NumberEditor) spinner.getEditor()).getTextField();
                formattedTextField.setEditable(true);
                DefaultFormatterFactory factory = (DefaultFormatterFactory) formattedTextField.getFormatterFactory();
                NumberFormatter formatter = (NumberFormatter) factory.getDefaultFormatter();
                // 设置输入限制生效
                formatter.setAllowsInvalid(false);
                return spinner;
            }
        });

        // 确认按钮
        confirmButton.addActionListener(new ActionListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void actionPerformed(ActionEvent e) {

                // 获取当前选择的表名
                String currentTableName = tableNameList.getSelectedItem().toString();

                // 什么也不做
                if ("-请选择-".equals(currentTableName)) {
                    return;
                }

                //如果当前表没有设置过实体属性
                if (ChildWindowConstant.currentTableNameAndTypes.get(currentTableName) == null) {
                    // 查询表名所有字段的名称和类型
                    List<TableNameAndType> CurrentTableNameAndTypes = DBUtils.getColumnNameAndTypes(
                            parameters.getDataBaseTypeVal(), currentTableName, DBUtils.getConnection(parameters));

                    //把当前表的字段名和类型设置到全局map中
                    ChildWindowConstant.currentTableNameAndTypes.put(currentTableName, CurrentTableNameAndTypes);
                }


                // 检查当前表的主键是否填写
                if ("".equals(primaryKeyListText.getText())) {
                    JOptionPane.showMessageDialog(confirmButton.getParent(), "请填写当前表的主键！如果没有主键，填写一个唯一键或其他任意列即可", "警告",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                }

                // 设置当前表的主键
                List<String> priamryKeyArr = Arrays.asList(primaryKeyListText.getText().split("#"));
                ChildWindowConstant.primaryKeyListMap.put(currentTableName, priamryKeyArr);
                // 设置当前表的中文名称
                String currentTableCnName = currentTableCn.getText().equals("") ? currentTableName
                        : currentTableCn.getText();
                ChildWindowConstant.currentTableCnNameMap.put(currentTableName, currentTableCnName);

                // 设置表的参数类型
                String selectParam = paramConfig.getSelectedItem().toString();
                ChildWindowConstant.tableParamConfig.put(CodeConstant.paramConfigKey, selectParam);

                Component[] components = columnNamePanel.getComponents();

                List<DatabaseModel> columnMsgList = new ArrayList<>();

                // 从第9个开始遍历
                for (int i = 10; i < components.length; i++) {

                    if ("JCheckBox".equals(components[i].getClass().getSimpleName())) {

                        JCheckBox checkBox = (JCheckBox) components[i];

                        DatabaseModel databaseModel = new DatabaseModel();
                        databaseModel.setCheckBox(checkBox);
                        columnMsgList.add(databaseModel);

                    } else if ("JTextField".equals(components[i].getClass().getSimpleName())) {

                        JTextField textField = (JTextField) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setTextField(textField);

                    } else if ("JSpinner".equals(components[i].getClass().getSimpleName())) {

                        JSpinner jSpinner = (JSpinner) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setjSpinner(jSpinner);

                    } else if ("ShowNumJSpinner".equals(components[i].getClass().getSimpleName())) {

                        ShowNumJSpinner showNumJSpinner = (ShowNumJSpinner) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setShowNumJSpinner(showNumJSpinner);

                    } else if ("JComboBox".equals(components[i].getClass().getSimpleName())) {

                        JComboBox<String> comboBox = (JComboBox<String>) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setComboBox(comboBox);

                    }

                    // 如果是业务类型下拉框
                    else if ("ServiceTypeComboBox".equals(components[i].getClass().getSimpleName())) {
                        ServiceTypeComboBox<String> comboBox = (ServiceTypeComboBox<String>) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setServiceTypeComboBox(comboBox);
                    }

                    // 如果是业务类型描述
                    else if ("ServiceJTextField".equals(components[i].getClass().getSimpleName())) {
                        ServiceJTextField textField = (ServiceJTextField) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setServiceJTextField(textField);

                    } else if ("CompareComboBox".equals(components[i].getClass().getSimpleName())) {

                        CompareComboBox<String> comboBox = (CompareComboBox<String>) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setCompareComboBox(comboBox);

                    } else if ("UpdateCheckBox".equals(components[i].getClass().getSimpleName())) {

                        UpdateCheckBox updateCheckBox = (UpdateCheckBox) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setUpdateCheckBox(updateCheckBox);

                    } else if ("QueryCheckBox".equals(components[i].getClass().getSimpleName())) {

                        QueryCheckBox queryCheckBox = (QueryCheckBox) components[i];
                        columnMsgList.get(columnMsgList.size() - 1).setQueryCheckBox(queryCheckBox);
                    }

                }

                // 前台需要展示的数据项
                List<DatabaseModel> selectColumnList = new ArrayList<>();

                // 前台更新用于展示的数据项
                List<DatabaseModel> updateColumnList = new ArrayList<>();

                // 前台查询用于展示的数据项
                List<DatabaseModel> queryColumnList = new ArrayList<>();

                // 以选中的checkBox为基准
                for (int i = 0; i < columnMsgList.size(); i++) {

                    DatabaseModel databaseModel = columnMsgList.get(i);

                    JCheckBox checkColumnName = databaseModel.getCheckBox();
                    JTextField textField = databaseModel.getTextField();
                    JSpinner jSpinner = databaseModel.getjSpinner();
                    JSpinner showNumJspinner = databaseModel.getShowNumJSpinner();
                    JComboBox<String> comboBox = databaseModel.getComboBox();
                    ServiceTypeComboBox<String> serviceTypeComboBox = databaseModel.getServiceTypeComboBox();
                    ServiceJTextField serviceJTextField = databaseModel.getServiceJTextField();
                    CompareComboBox<String> compareComboBox = databaseModel.getCompareComboBox();
                    UpdateCheckBox updateCheckBox = databaseModel.getUpdateCheckBox();
                    QueryCheckBox queryCheckBox = databaseModel.getQueryCheckBox();

                    String columnEng = checkColumnName.getText();
                    databaseModel.setColumnsEng(columnEng);
                    databaseModel.setColumnsCn("".equals(textField.getText()) ? columnEng : textField.getText());
                    databaseModel.setWhereNo((Integer) jSpinner.getValue());
                    databaseModel.setShowNo((Integer) showNumJspinner.getValue());
                    databaseModel.setCanSort((String) comboBox.getSelectedItem());

                    String selectedItem = (String) serviceTypeComboBox.getSelectedItem();
                    databaseModel.setServiceType(selectedItem);

                    String serviceText = serviceJTextField.getText();

                    databaseModel.setServiceTextStr(serviceText);

                    String[] serviceArr = serviceText.split("#");

                    databaseModel.setCompareValue((String) compareComboBox.getSelectedItem());

                    if (checkColumnName.isSelected()) {
                        if (!checkType(confirmButton, databaseModel, selectedItem, serviceArr)) {
                            JOptionPane.showMessageDialog(confirmButton.getParent(),
                                    columnEng + "的类型描述填写有误，请检查后按照配置说明重新填写！", "错误", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        selectColumnList.add(databaseModel);
                    }

                    if (checkColumnName.isSelected() && updateCheckBox.isSelected()) {
                        updateColumnList.add(databaseModel);
                    }

                    if (queryCheckBox.isSelected()) {
                        if (!checkType(confirmButton, databaseModel, selectedItem, serviceArr)) {
                            JOptionPane.showMessageDialog(confirmButton.getParent(),
                                    columnEng + "的类型描述填写有误，请检查后按照配置说明重新填写！", "错误", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        queryColumnList.add(databaseModel);
                    }

                }

                // 设置完毕后查看checkBox的集合是否为空
                if (selectColumnList.size() == 0) {

                    JOptionPane.showMessageDialog(confirmButton.getParent(),
                            "你没有选择表 " + currentTableName + " 需要展示的数据项！生成时默认为你全选且中文名为英文字段名称！", "警告",
                            JOptionPane.WARNING_MESSAGE);

                }

                if (updateColumnList.size() == 0) {

                    JOptionPane.showMessageDialog(confirmButton.getParent(), "你没有选择页面 更新功能 需要展示的数据项！生成时将以展示的数据项为准！",
                            "警告", JOptionPane.WARNING_MESSAGE);

                }

                if (queryColumnList.size() == 0) {

                    JOptionPane.showMessageDialog(confirmButton.getParent(), "你没有选择页面 查询功能 需要展示的数据项！生成时将以展示的数据项为准！",
                            "警告", JOptionPane.WARNING_MESSAGE);

                }

                JOptionPane.showMessageDialog(confirmButton.getParent(), "表 " + currentTableName + " 数据项配置成功！", "提示",
                        JOptionPane.INFORMATION_MESSAGE);

                // 展示项的信息
                ChildWindowConstant.columnMsgMap.put(currentTableName, selectColumnList);
                // 更新项的信息
                ChildWindowConstant.updateColumnMsgMap.put(currentTableName, updateColumnList);
                // 查询项的信息
                ChildWindowConstant.queryColumnMsgMap.put(currentTableName, queryColumnList);

                // 所有项的信息
                ChildWindowConstant.allColumnMsgMap.put(currentTableName, columnMsgList);

            }

            private boolean checkType(final JButton confirmButton, DatabaseModel databaseModel, String selectedItem,
                                      String[] serviceArr) {
                Map<String, Object> map = new HashMap<>();
                if ("布尔".equals(selectedItem)) {
                    try {
                        map.put("是", serviceArr[0]);
                        map.put("否", serviceArr[1]);
                        databaseModel.setServiceText(map);
                    } catch (Exception e1) {
                        return false;
                    }
                }

                if ("状态码".equals(selectedItem)) {
                    try {
                        for (String serviece : serviceArr) {
                            String[] arr = serviece.split("&");
                            map.put(arr[0], arr[1]);
                        }
                        databaseModel.setServiceText(map);
                    } catch (Exception e1) {
                        return false;
                    }
                }
                return true;
            }
        });

        getContentPane().setLayout(groupLayout);
    }
}
