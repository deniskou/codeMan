package codeMaker;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import constant.ChildWindowConstant;

public class VxGzhCode extends JFrame {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	class ImagePanel extends JPanel {
		/**
		 * serialVersionUID
		 */
		private static final long serialVersionUID = 1L;
		Dimension d;
		Image image;

		public ImagePanel(Dimension d, Image image) {
			super();
			this.d = d;
			this.image = image;
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(image, 0, 0, d.width, d.height, this);
			this.repaint();
		}
	}

	Dimension frameSize = new Dimension(500, 500);
	ImageIcon imageIcon = new ImageIcon(
			Toolkit.getDefaultToolkit().getImage(VxGzhCode.class.getResource("/codeMaker/vxCode.jpg")));

	public VxGzhCode() {
		setResizable(false);
		setTitle("关注");
		// 设置窗体属性
		setSize((int) frameSize.getWidth(), (int) frameSize.getHeight());
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VxGzhCode.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		setUndecorated(true);
	}

	public void addImageByJLable() {
		getContentPane().setLayout(null);

		// 设置背景
		JLabel lbBg = new JLabel(imageIcon);
		lbBg.setBounds(0, 0, frameSize.width, frameSize.height);
		this.getContentPane().add(lbBg);

		setVisible(true);
	}

	public void addImageByRepaint() {
		ImagePanel imagePanel = new ImagePanel(frameSize, imageIcon.getImage());
		setContentPane(imagePanel);

		setVisible(true);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VxGzhCode imageFrame = new VxGzhCode();
		// imageFrame.addImageByJLable();
		imageFrame.addImageByRepaint();

		ChildWindowConstant.vxGzhCode = imageFrame;
	}

}