jdbc.driver=${dataBaseDriverClass!""}
jdbc.url=${dataBaseUrl!""}
jdbc.username=${dataBaseUserName!""}
jdbc.password=${dataBasePwd!""}

<#if databasePool == "Druid">
jdbc.filters=stat
jdbc.maxActive=20
jdbc.initialSize=1
jdbc.maxWait=60000
jdbc.minIdle=10
jdbc.timeBetweenEvictionRunsMillis=60000
jdbc.minEvictableIdleTimeMillis=300000
jdbc.validationQuery=SELECT 'x'
jdbc.testWhileIdle=true
jdbc.testOnBorrow=false
jdbc.testOnReturn=false
jdbc.maxOpenPreparedStatements=20
jdbc.removeAbandoned=true
jdbc.removeAbandonedTimeout=1800
jdbc.logAbandoned=true
</#if>

<#if databasePool == "HikariCP">
jdbc.minimum-idle=5
jdbc.idle-timeout=180000
jdbc.maximum-pool-size=10
jdbc.auto-commit=true
jdbc.max-lifetime=1800000
jdbc.connection-timeout=30000
jdbc.connection-test-query=SELECT 1
</#if>