package ${packageName}.service.impl;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${packageName}.dao.${IDaoName};
import ${packageName}.service.${IServiceName};
import ${packageName}.entity.PageData;
import ${packageName}.utils.ExcelUtil;
import ${packageName}.utils.PageUtil;
<#if entityName??>
import java.util.LinkedHashMap;
import ${packageName}.entity.${entityName};
</#if>
import java.util.List;
import java.util.Map;
import ${packageName}.utils.PageUtil;

@Service
<#if !entityName??>
public class ${serviceName} implements ${IServiceName} {

	
	private ${IDaoName} dao;

	@Autowired
	public ${serviceName}(${IDaoName} dao) {
		this.dao = dao;
	}
	
	@Override
	public void add(Map<String, Object> map) {
		dao.add(map);
	}
	
	@Override
	public void delete(Map<String, Object> map) {
		dao.delete(map);
	}
	
	@Override
	public void update(Map<String, Object> map) {
		dao.update(map);
	}
	
	@Override
	public List<Map<String,Object>> select(Map<String, Object> map) {
		return dao.select(map);
	}
	
	@Override
	public Map<String, Object> likeSelect(Map<String, Object> map) {
	
		return PageUtil.getPageData(map, dao);
	}

	@Override
	public void batchAdd(List<Map<String, Object>> list) {
		dao.batchAdd(list);
	}

	@Override
	public void batchDelete(List<Map<String, Object>> list) {
		dao.batchDelete(list);
	}

	@Override
	public void batchUpdate(List<Map<String, Object>> list) {
		dao.batchUpdate(list);
	}
	
	@Override
	public void exportExcel(Map<String, Object> paramMap, HttpServletResponse response) {

		// 获取头部信息
		String[] headList = new String[] {<#list selectColumnList as data> "${data.columnsCn}"<#if data_has_next>,</#if></#list>};

		String[] describeList = new String[] {<#list selectColumnList as data> "${data.serviceTextStr}"<#if data_has_next>,</#if></#list>};

		ExcelUtil.exportExcel(paramMap, response, dao, headList, describeList);
	}
}
<#else>
public class ${serviceName} implements ${IServiceName} {

	
	private ${IDaoName} dao;

	@Autowired
	public ${serviceName}(${IDaoName} dao) {
		this.dao = dao;
	}
	
	@Override
	public void add(${entityName} entity) {
		dao.add(entity);
	}
	
	@Override
	public void delete(${entityName} entity) {
		dao.delete(entity);
	}
	
	@Override
	public void update(${entityName} entity) {
		dao.update(entity);
	}
	
	@Override
	public List<${entityName}> select(${entityName} entity) {
		return dao.select(entity);
	}
	
	@Override
	public PageData<${entityName}> likeSelect(${entityName} entity) {
	
		return PageUtil.getPageData(entity, dao);
	}

	@Override
	public void batchAdd(List<${entityName}> list) {
		dao.batchAdd(list);
	}

	@Override
	public void batchDelete(List<${entityName}> list) {
		dao.batchDelete(list);
	}

	@Override
	public void batchUpdate(List<${entityName}> list) {
		dao.batchUpdate(list);
	}
	
	
	@Override
	public void exportExcel(${entityName} entity, HttpServletResponse response) {

		// 获取头部信息（可以设置为动态）
		String[] headList = new String[] {<#list selectColumnList as data> "${data.columnsCn}"<#if data_has_next>,</#if></#list>};
		
		String[] headEngList = new String[]{<#list selectColumnList as data> "${data.columnsEng}"<#if data_has_next>,</#if></#list>};

		String[] describeList = new String[] {<#list selectColumnList as data> "${data.serviceTextStr}"<#if data_has_next>,</#if></#list>};
		
		//设置头部以及描述信息
        Map<String, String> headAndDescribeMap = new LinkedHashMap<>();
        for (int i = 0; i < headEngList.length; i++) {
            headAndDescribeMap.put(headEngList[i], describeList[i]);
        }

		ExcelUtil.exportExcel(entity, response, dao, headList, headAndDescribeMap);
	}
}
</#if>
