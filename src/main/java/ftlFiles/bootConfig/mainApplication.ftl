package ${projectName};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ${captureProjectName}Application {

	public static void main(String[] args) {
		SpringApplication.run(${captureProjectName}Application.class, args);
	}

}
