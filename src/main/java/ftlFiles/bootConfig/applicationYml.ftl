server: 
  servlet:
    context-path: /${projectName}
    session:
      timeout: 1440m
spring:
  datasource:
    username: ${dataBaseUserName!""}
    password: ${dataBasePwd!""}
    url: ${dataBaseUrl!""}
    driver-class-name: ${dataBaseDriverClass!""}
    <#if databasePool == "Druid">
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      #访问 http://ip:port/工程名/druid/ 即可去往监控界面
      #配置初始化大小/最小/最大
      initial-size: 20
      min-idle: 10
      #最大活跃连接数
      max-active: 100
      #获取连接等待超时时间
      max-wait: 10000
      #间隔多久进行一次检测，检测需要关闭的空闲连接
      time-between-eviction-runs-millis: 60000
      #一个连接在池中最小生存的时间
      min-evictable-idle-time-millis: 300000
      validation-query: SELECT 'x'
      test-while-idle: true
      test-on-borrow: false
      test-on-return: false
      #打开PSCache，并指定每个连接上PSCache的大小。oracle设为true，mysql设为false。分库分表较多推荐设置为false
      pool-prepared-statements: false
      max-pool-prepared-statement-per-connection-size: 20
      stat-view-servlet:
        login-username: admin
        login-password: 123456
      #超时回收（耗费性能，但可有效防止连接泄露）
      remove-abandoned: true
      remove-abandoned-timeout: 1800
      log-abandoned: true
    </#if>
    <#if databasePool == "HikariCP">
    #spring官方推荐 性能最强，没有之一
    hikari:
      pool-name: Retail_HikariCP
      minimum-idle: 5 #最小空闲连接数量
      idle-timeout: 180000 #空闲连接存活最大时间，默认600000（10分钟）
      maximum-pool-size: 10 #连接池最大连接数，默认是10
      auto-commit: true  #此属性控制从池返回的连接的默认自动提交行为,默认值：true
      max-lifetime: 1800000 #此属性控制池中连接的最长生命周期，值0表示无限生命周期，默认1800000即30分钟
      connection-timeout: 30000 #数据库连接超时时间,默认30秒，即30000
      connection-test-query: SELECT 1
    </#if>
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
mybatis:
  config-location: classpath:sqlmapper/mybatis-config.xml
  mapper-locations: classpath:sqlmapper/*Mapper.xml
<#if ifUseSwagger == "是">
#是否显示swagger
swagger:
  enabled: true
</#if>