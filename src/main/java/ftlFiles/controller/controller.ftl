package ${packageName}.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.entity.CommonResult;
import ${packageName}.constant.ResultConstant;
import ${packageName}.service.${IServiceName};

<#if ifUseSwagger == "是">
import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>

<#if !entityName??>
import java.util.Map;
import org.springframework.web.bind.annotation.RequestParam;
<#else>
import ${packageName}.entity.${entityName};
</#if>
import java.util.List;

<#if !entityName??>
@RestController
@CrossOrigin(origins = "*",allowCredentials = "true",allowedHeaders = "*")
<#if ifUseSwagger == "是">
@Api(tags = "${controllerPrefix}接口")
</#if>
@RequestMapping("/${controllerPrefix}")
public class ${controllerName} {

	
	private ${IServiceName} service;

	@Autowired
	public ${controllerName}(${IServiceName} service) {
		this.service = service;
	}
	
	/**
	 * 查询
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "查询")
	</#if>
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public CommonResult select(@RequestBody Map<String, Object> map) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.select(map));
	}

	/**
	 * 模糊查询
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "模糊查询")
	</#if>
	@RequestMapping(value = "/likeSelect", method = RequestMethod.POST)
	public CommonResult likeSelect(@RequestBody Map<String, Object> map) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.likeSelect(map));
	}

	/**
	 * 更新
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "更新")
	</#if>
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public CommonResult update(@RequestBody Map<String, Object> map) {
		service.update(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "添加")
	</#if>
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public CommonResult add(@RequestBody Map<String, Object> map) {
		service.add(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "删除")
	</#if>
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public CommonResult delete(@RequestBody Map<String, Object> map) {
		service.delete(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量增加
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping(value = "/batchAdd", method = RequestMethod.POST)
	public CommonResult batchAdd(@RequestBody List<Map<String, Object>> list) {
		service.batchAdd(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量删除
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public CommonResult batchDelete(@RequestBody List<Map<String, Object>> list) {
		service.batchDelete(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量更新
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping(value = "/batchUpdate", method = RequestMethod.POST)
	public CommonResult batchUpdate(@RequestBody List<Map<String, Object>> list) {
		service.batchUpdate(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}	
	
	/**
	 * 导出excel
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping("/exportExcel")
	public void exportExcel(@RequestParam Map<String, Object> paramMap, HttpServletResponse response) {
		service.exportExcel(paramMap, response);
	}
					
}
<#else>
@RestController
@CrossOrigin(origins = "*",allowCredentials = "true",allowedHeaders = "*")
<#if ifUseSwagger == "是">
@Api(tags = "${controllerPrefix}接口")
</#if>
@RequestMapping("/${controllerPrefix}")
public class ${controllerName} {


	private ${IServiceName} service;

	@Autowired
	public ${controllerName}(${IServiceName} service) {
		this.service = service;
	}
	
	/**
	 * 查询
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "查询")
	</#if>
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public CommonResult select(@RequestBody ${entityName} entity) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.select(entity));
	}

	/**
	 * 模糊查询
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "模糊查询")
	</#if>
	@RequestMapping(value = "/likeSelect", method = RequestMethod.POST)
	public CommonResult likeSelect(@RequestBody ${entityName} entity) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.likeSelect(entity));
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "更新")
	</#if>
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public CommonResult update(@RequestBody ${entityName} entity) {
		service.update(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "添加")
	</#if>
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public CommonResult add(@RequestBody ${entityName} entity) {
		service.add(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "删除")
	</#if>
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public CommonResult delete(@RequestBody ${entityName} entity) {
		service.delete(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量增加
	 *
	 * @return
	 */
    <#if ifUseSwagger == "是">
    @ApiIgnore
    </#if>
	@RequestMapping(value = "/batchAdd", method = RequestMethod.POST)
	public CommonResult batchAdd(@RequestBody List<${entityName}> list) {
		service.batchAdd(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量删除
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public CommonResult batchDelete(@RequestBody List<${entityName}> list) {
		service.batchDelete(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量更新
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping(value = "/batchUpdate", method = RequestMethod.POST)
	public CommonResult batchUpdate(@RequestBody List<${entityName}> list) {
		service.batchUpdate(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}
	
	/**
	 * 导出excel
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiIgnore
	</#if>
	@RequestMapping("/exportExcel")
	public void exportExcel(${entityName} entity, HttpServletResponse response) {
		service.exportExcel(entity, response);
	}

}
</#if>