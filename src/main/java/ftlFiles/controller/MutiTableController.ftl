package ${packageName}.controller;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.constant.ResultConstant;
import ${packageName}.entity.CommonResult;
<#list currentMethodMap?keys as key>
import ${packageName}.entity.${currentMethodMap["${key}"].entityName?cap_first}Muti;
</#list>
import ${packageName}.service.I${capCurrentMutiEng}MutiService;
<#if ifUseSwagger == "是">
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
</#if>

@RestController
@CrossOrigin(origins = "*",allowCredentials = "true",allowedHeaders = "*")
<#if ifUseSwagger == "是">
@Api(tags = "${currentMutiCn}接口")
</#if>
@RequestMapping("/${currentMutiEng}Muti")
public class ${capCurrentMutiEng}MutiController {

	@Autowired
	public I${capCurrentMutiEng}MutiService service;


	<#list currentMethodMap?keys as key>
		<#assign methodName = currentMethodMap["${key}"].methodName_cn/>
		<#assign entityName = currentMethodMap["${key}"].entityName/>
	/**
	 * ${methodName}
	 *
	 * @return
	 */
    <#if ifUseSwagger == "是">
    @ApiOperation(value = "${methodName}")
    </#if>
	@RequestMapping(value = "/${key}", method = RequestMethod.POST)
	public CommonResult ${key}(@RequestBody ${entityName?cap_first}Muti entity) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.${key}(entity));
	}

	/**
	 * 导出excel
	 *
	 * @return
	 */
    <#if ifUseSwagger == "是">
    @ApiIgnore
    </#if>
	@RequestMapping("/${key}ExportExcel")
	public void ${key}ExportExcel(${entityName?cap_first}Muti entity, HttpServletResponse response) {
		service.${key}ExportExcel(entity, response);
	}

	</#list>

}
