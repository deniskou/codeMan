package ${packageName}.entity;

<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;
<#list entityNameAndTypes as nameAndTypeAndComment>
<#--如果className不为空-->
<#if nameAndTypeAndComment.className !="">
import ${nameAndTypeAndComment.className};
</#if>
</#list>

<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class ${entityName} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list entityNameAndTypes as nameAndTypeAndComment>
    /**
	 *  ${nameAndTypeAndComment.comment}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${nameAndTypeAndComment.comment}", name = "${nameAndTypeAndComment.name}")
	</#if>
    private ${nameAndTypeAndComment.typeName} ${nameAndTypeAndComment.name};
</#list>

<#list entityNameAndTypes as nameAndTypeAndComment>
    /**
	 *  get ${nameAndTypeAndComment.comment}
	 */
    public ${nameAndTypeAndComment.typeName} get${nameAndTypeAndComment.name?cap_first}() {
		return ${nameAndTypeAndComment.name};
	}

    /**
	 *  set ${nameAndTypeAndComment.comment}
	 */
	public void set${nameAndTypeAndComment.name?cap_first}(${nameAndTypeAndComment.typeName} ${nameAndTypeAndComment.name}) {
		this.${nameAndTypeAndComment.name} = ${nameAndTypeAndComment.name};
	}
</#list>

}
