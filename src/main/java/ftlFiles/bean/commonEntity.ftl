package ${packageName}.entity;

import java.util.List;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModelProperty;
</#if>

/**
 * 实体类都继承的类
 */
public class CommonEntity {
    /**
     * 前台的排序条件
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private List<String> orderData;

    /**
     * 当前页
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(value = "当前页",name = "currentPage",required = true)
    </#if>
    private Integer currentPage;

<#if dataBaseType == "mysql" || dataBaseType == "postgresql">
    /**
     * 开始下标
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer start;

    /**
     * 每页显示条数
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer pageSize;
</#if>

<#if dataBaseType == "oracle">

	<#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer startIndex;

	<#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer endIndex;
</#if>
    /**
     * 字符串格式的排序条件
     */
    @ApiModelProperty(hidden = true)
    private String orderStr;

    public List<String> getOrderData() {
        return orderData;
    }

    public void setOrderData(List<String> orderData) {
        this.orderData = orderData;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
<#if dataBaseType == "mysql"  || dataBaseType == "postgresql">
    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
</#if>
    public String getOrderStr() {
        return orderStr;
    }

    public void setOrderStr(String orderStr) {
        this.orderStr = orderStr;
    }
<#if dataBaseType == "oracle">

	public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }
</#if>
}

