package ${packageName}.entity;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;

/**
 * 公共返回的数据
 */
<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class CommonResult implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
	
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "状态码", name = "code")
	</#if>
    private int code;

	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "返回信息", name = "msg")
	</#if>
    private String msg;
    
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "结果信息", name = "result")
	</#if>
    private Object result;

    public CommonResult(int code, String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public CommonResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
