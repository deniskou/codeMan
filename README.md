# 当前版本
代码生成器已更新至v2.10版，新增前后端分离响应式主题，修复了用户反馈的若干bug！<br>
所具功能：<br>
1.数据库：MySql/PostgreSql/Oracle<br>
2.后台：SSM/SpringBoot<br>
3.前台：BootStrap/Jquery/Vue<br>
4.后台controller/service/dao/entity/mapper，前台html/js/css一键生成，并抽取公共基类<br>
5.单表、多表查询，自定义字段及其类型，支持Map/Bean的参数类型<br>
6.集成logback日志<br>
7.集成日志切面组件，日志工具类<br>
8.统一异常处理类，统一响应处理类<br>
9.统一事务管理<br>
10.跨域请求支持<br>
11.自定义参数设置：连接池、swagger<br>
12.支持单体应用生成<br>
13.支持前后端分离，移动端适配应用生成<br>
后续更新：<br>
1.常用组件支持：如MongoDb，Redis，ElasticSearch，RabbitMq等<br>
2.SpringCloud框架支持<br>
3.多数据源模式<br>
。。。敬请期待！<br>
# 运行效果
![运行截图1](https://images.gitee.com/uploads/images/2020/0421/005722_6ea2a18c_5025976.png "QQ截图20200421010532.png")<br>
![运行截图2](https://images.gitee.com/uploads/images/2020/0421/005741_a4fc168f_5025976.png "QQ截图20200421010546.png")<br/>
# 最新版生成的代码结构及页面展示
后台代码结构：<br>
![后台代码结构](https://images.gitee.com/uploads/images/2020/0421/014310_63445fb2_5025976.png "QQ截图20200421015545.png")<br>
前台代码结构：<br>
![前台代码结构](https://images.gitee.com/uploads/images/2020/0421/011118_c14cdf9f_5025976.png "QQ截图20200421012406.png")<br>
页面展示：<br>
![登录页](https://images.gitee.com/uploads/images/2020/0421/013825_8625c295_5025976.png "QQ截图20200421014925.png")<br>
![主页](https://images.gitee.com/uploads/images/2020/0421/013849_139d8d61_5025976.png "QQ截图20200421015058.png")
# 推荐源码的查看方式
全部的代码较为杂乱，推荐直接从**main**包下的**MainMethod**看起，这是代码生成器的核心逻辑部分，加载了**freeMarker模板**以及**设置模板中的参数**，**ftlFiles**文件夹下存放了所有的**freeMarker模板**。<br>
**codeMaker**包下的**LifeCode**是生成器的入口类，**DataBaseConfig**是数据库配置的入口类，**TablesQuery**是多表查询配置的入口类。<br>
以上便是代码生成器比较核心的部分。
# 程序版运行方式
从码云上把项目下载下来之后直接导入**idea/eclipse**等待加载完maven依赖之后便可运行（入口类：codeMaker.LifeCode）,有一点需要注意：生成器运行需要依赖ftl模板以及一些配置文件，所以需要把源码中的codeManConfig文件夹复制到C盘根目录下才可正常运行！
# idea插件版快速运行方式
根据当前系统把codeManPlugins.zip安装到idea中，安装方法：<br/>
File -》setting -》Plugins -》install plugins from disk，选择对应的压缩包确定即可，重启便可在idea的window菜单栏使用！
# 自动运行方式
不习惯使用idea的朋友可以直接下载[最新版生成器](https://gitee.com/zrxjava/code_generator_v201)，分为mac和windows两个版本，windows下解压运行exe程序，mac下运行command程序即可！
# 生成后代码的运行方式
目前有两款主题可选：经典后台Thymleaf版 和 前后端分离响应式。<br>
若选择 经典后台Thymleaf版主题 直接把生成的项目导入idea/eclipse，等待maven依赖引入完毕之后便可直接运行访问。<br>
若选择  前后端分离响应式主题 会生成两个项目：前台项目和后台项目，后台项目导入idea/eclipse运行，等待后台项目启动完毕，可以直接用浏览器打开前台项目文件夹中的login.html与后台交互，也可选择把前台项目部署到nginx/apache来进行访问。